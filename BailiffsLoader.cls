VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "BailiffsLoader"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
''
'BailiffsLoader
'
'loads list of bailiffs from https://www.ekcr.cz
'stores the table of baillifs to the hidden sheet
'
'@author Radim Nenal
''

Const SOURCEURL As String = "https://www.ekcr.cz/seznam-exekutoru"
Private DestTableName As String
Private A1 As Range

''
'Like constructor, see the Factory module
''
Public Sub Init(rangeA1 As Range, strDestTableName As String)
    Set A1 = rangeA1
    DestTableName = strDestTableName

    ReloadExecutors
    
End Sub

Private Function SplitTitles(titles As String) As String()
    Dim position As Integer: position = 0
    Dim tmppos As Integer
    Dim afterNameTitles() As Variant
    afterNameTitles = Array("Ph.D.", "Th.D.", "CSc.", "DrSc.", "DSc.", "DiS.", "Dr.h.c.", "LL.M.", "MBA")
    Dim arrToReplace() As Variant
    arrToReplace = Array("Ph. D.", "Th. D.", "CSc.", "DrSc.", "DSc.", "DiS.", "Dr. h. c.", "LL. M.", "MBA")
    
    Dim i As Integer
    For i = LBound(arrToReplace) To UBound(arrToReplace)
        titles = Replace(titles, arrToReplace(i), afterNameTitles(i))
    Next
    
    Dim title As Variant
    For Each title In afterNameTitles
        tmppos = InStr(titles, title)
        If tmppos > 0 Then
            If tmppos < position Or position = 0 Then
                position = tmppos
            End If
        End If
    Next
    
    Dim output(0 To 1) As String
    If position > 0 Then
        output(0) = Trim(Left(titles, position - 1))
        output(1) = Trim(Right(titles, Len(titles) - position + 1))
    Else
        output(0) = Trim(titles)
        output(1) = ""
    End If
    
    SplitTitles = output
    
End Function

Private Function ParseHtml() As Collection
    Dim items As Collection: Set items = New Collection
    
    Dim http As Object: Set http = CreateObject("MSXML2.XMLHTTP")
    http.Open "GET", SOURCEURL, False
    http.send
    
    Dim html As New HTMLDocument
    html.body.innerHTML = http.responseText
    
    Dim topics As Object: Set topics = html.getElementById("result").Children
    Dim topic As HTMLHtmlElement
    
    For Each topic In topics
        
        Dim blocks As Object: Set blocks = topic.Children
        Dim block_1 As HTMLHtmlElement: Set block_1 = blocks(0)
        Dim block_tel As HTMLHtmlElement: Set block_tel = blocks(2)
        Dim block_numbers As HTMLHtmlElement: Set block_numbers = blocks(4)
        
        Dim DataRow As ExecutorInitials: Set DataRow = New ExecutorInitials
        
        Dim lines() As String
        lines = Split(block_1.innerText, vbNewLine, 4)
        DataRow.district = Split(lines(0), ": ", 2)(1)
        Dim arrayFullname() As String: arrayFullname = Split(lines(1), ", ")
        DataRow.firstname = arrayFullname(1)
        Dim lastname As String: lastname = arrayFullname(0)
        Dim arrayLastname() As String: arrayLastname = Split(lastname, " ", 2)
        DataRow.lastname1 = arrayLastname(0)
        DataRow.lastname2 = ""
        If UBound(arrayLastname) > 0 Then
            DataRow.lastname2 = arrayLastname(1)
        End If
        Dim titles As String: titles = Split(arrayFullname(2), " (", 2)(0)
        Dim arrayTitles() As String: arrayTitles = SplitTitles(titles)
        DataRow.beforeNameTitles = arrayTitles(0)
        DataRow.afterNameTitles = arrayTitles(1)
        DataRow.street = lines(2)
        DataRow.city = lines(3)
        
        lines = Split(block_tel.innerText, vbNewLine, 2)
        DataRow.telephone = lines(0)
        
        lines = Split(block_numbers.innerText, vbNewLine, 3)
        DataRow.idNumber = lines(0)
        DataRow.companyIdNumber = lines(1)
        DataRow.dataBoxId = lines(2)

        items.Add DataRow
        
    Next
    
    Set ParseHtml = items
    
End Function

Public Sub ReloadExecutors()
    Application.ScreenUpdating = False
    
    Dim items As Collection
    Set items = ParseHtml
    
    On Error Resume Next
        With A1.Worksheet.ListObjects(DestTableName)
            .DataBodyRange.Delete
            .Unlist
        End With
    On Error GoTo 0
    
    Dim row As Integer: row = 2
    
    For Each Item In items
        A1.Cells(row, 1).value = "'" & Item.idNumber
        A1.Cells(row, 2).value = Item.district
        A1.Cells(row, 3).value = Item.beforeNameTitles
        A1.Cells(row, 4).value = Item.firstname
        A1.Cells(row, 5).value = Item.lastname1
        A1.Cells(row, 6).value = Item.lastname2
        A1.Cells(row, 7).value = Item.afterNameTitles
        A1.Cells(row, 8).value = Item.street
        A1.Cells(row, 9).value = Item.city
        A1.Cells(row, 10).value = "'" & Item.telephone
        A1.Cells(row, 11).value = "'" & Item.companyIdNumber
        A1.Cells(row, 12).value = "'" & Item.dataBoxId
       
        row = row + 1

    Next

    Dim src As Range: Set src = A1.CurrentRegion
    A1.Worksheet.ListObjects.Add(SourceType:=xlSrcRange, Source:=src, _
            xlListObjectHasHeaders:=xlYes, tablestyleName:="TableStyleMedium28").Name = DestTableName

    Application.ScreenUpdating = True
End Sub



