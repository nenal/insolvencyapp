VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ClientDataService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
''
'ClientDataService
'provides especially loading and saving user data  (e.g. name, address etc.)
'
'@author Radim Nenal
''

Private ClientVariablesTable As ListObject          'the UI table user data (e.g. name, address etc.)
Private EnforcementTable As ListObject              'hidden table which serves as mailmerge source
Private ClientMailMergeTable As ListObject          'another hidden which serves as mailmerge source
Private JsonDataFilename As String                  'file to store user data (e.g. name, address etc.)

''
'Like constructor, see the Factory module
''
Public Sub Init(objClientVariablesTable As ListObject, _
        objEnforcementTable As ListObject, _
        objClientMailMergeTable As ListObject, _
        strJsonDataFilename As String)
    Set ClientVariablesTable = objClientVariablesTable
    Set EnforcementTable = objEnforcementTable
    Set ClientMailMergeTable = objClientMailMergeTable
    JsonDataFilename = strJsonDataFilename
    
End Sub

Private Function SpecialTypeCellValue(valueType As String, value As String)
    If valueType = "date" Then
        On Error GoTo WrongFormatOfDate
        SpecialTypeCellValue = JsonConverter.ParseIso(value)
        On Error GoTo 0
    Else
        SpecialTypeCellValue = value
    End If
    Exit Function

WrongFormatOfDate:
    SpecialTypeCellValue = value

End Function

''
'Function for finding a value in a first column of a ListObject (i.e. table)
'If it finds the value, returns ListRow object
'The function is not used yet, planning to use it in the nearest version
''
Private Function LookupRow(Tbl As ListObject, LookupValue As String) As ListRow

    Dim FoundCell As Range

    'Attempt to find value in Table's first Column
    On Error Resume Next
        Set FoundCell = Tbl.DataBodyRange.Columns(1).Find(LookupValue, LookAt:=xlWhole)
    On Error GoTo 0
    
    'Return Table Row [not his number] if value is found
    If Not FoundCell Is Nothing Then
        'Set LookupRow = Tbl.ListRows(FoundCell.row - Tbl.HeaderRowRange.row)
        Set LookupRow = FoundCell.row
    Else
        Set LookupRow = Nothing
    End If

End Function

''
'Loads client data from json file
'The main reason for loading data from an external file is to facilitate application updates
'Excuse the length of the code block, should be refactored
''
Public Sub LoadJson()
    Dim fso As New FileSystemObject
    Dim jsonTS As TextStream
    Dim jsonText As String
    
    Application.ScreenUpdating = False
    
    Set jsonTS = fso.OpenTextFile(ThisWorkbook.Path & "\" & JsonDataFilename, ForReading)
    jsonText = jsonTS.ReadAll
    jsonTS.Close
    
    Dim dataDict As Scripting.Dictionary
    Set dataDict = JsonConverter.ParseJson(jsonText)

    Dim templateVariables As Scripting.Dictionary
    Set templateVariables = dataDict.Item("template_variables")
    
    Dim mailMergeClientDataSheet As Worksheet
    Set mailMergeClientDataSheet = ClientMailMergeTable.Range.Worksheet
    Dim ClientMailMergeTableName As String
    ClientMailMergeTableName = ClientMailMergeTable.Name
    
    On Error Resume Next
        ClientVariablesTable.DataBodyRange.Delete 'ne!!!
    On Error GoTo 0

    On Error Resume Next
        ClientMailMergeTable.Delete
    On Error GoTo 0
    
    Dim i As Integer
    For i = 0 To templateVariables.Count - 1
        Dim attributes As Scripting.Dictionary
        Set attributes = templateVariables.items(i)

        Dim newrow As ListRow
        Set newrow = ClientVariablesTable.ListRows.Add
        With newrow
            Select Case attributes.Item("type")
                Case "date"
                    .Range(2).numberFormat = "D.M.YYYY;@"
                Case "string", "text"
                    .Range(2).numberFormat = "@"
                Case "float"
                    .Range(2).numberFormat = "0.00"
                Case "czk"
                    .Range(2).numberFormat = "# ##0,00 K�"
                Case "phone"
                    .Range(2).numberFormat = "### ### ###;@"
                Case Else
                    .Range(2).numberFormat = attributes.Item("type")
            End Select

            .Range(1).value = attributes.Item("title")
            .Range(2).value = SpecialTypeCellValue(attributes.Item("type"), attributes.Item("value"))
            .Range(3).value = attributes.Item("note")
            .Range(4).value = attributes.Item("type")
            .Range(5).value = templateVariables.Keys(i)
        End With
            
        mailMergeClientDataSheet.Cells(1, i + 1).value = templateVariables.Keys(i)
        
        Dim cellAddress As String, formulaCellFormat As String, formula As String
        cellAddress = newrow.Range(2).Parent.Name & "!" & newrow.Range(2).Address(external:=False)
        formulaCellFormat = newrow.Range(2).numberFormat
        If attributes.Item("type") = "date" Then
            formulaCellFormat = "D. M. RRRR;@"
        End If
        formula = "=KDY�(" & cellAddress & "="""";"""";" & "HODNOTA.NA.TEXT(" & cellAddress & ";""" & formulaCellFormat & """))"
        mailMergeClientDataSheet.Cells(2, i + 1).FormulaLocal = formula
    Next
    
    Dim src As Range: Set src = mailMergeClientDataSheet.Range("A1").CurrentRegion
    mailMergeClientDataSheet.ListObjects.Add(SourceType:=xlSrcRange, Source:=src, _
            xlListObjectHasHeaders:=xlYes, tablestyleName:="TableStyleMedium28").Name = ClientMailMergeTableName
    
    If Not EnforcementTable.DataBodyRange Is Nothing Then
        EnforcementTable.DataBodyRange.Delete
    End If
    
    Dim enforcementFilemarks As Collection
    Set enforcementFilemarks = dataDict.Item("enforcement_filemarks")
    Dim strFilemark As Variant
    For Each strFilemark In enforcementFilemarks
        Set newrow = EnforcementTable.ListRows.Add
        newrow.Range = strFilemark
    Next
    
    Application.ScreenUpdating = True

End Sub

Private Sub JsonWriteToFile(Filename As String, jsonText As String)
    Dim fso As New FileSystemObject
    Dim jsonTS As TextStream
    
    Set jsonTS = fso.OpenTextFile(Filename, ForWriting, True)
    jsonTS.Write jsonText
    jsonTS.Close
    
End Sub

Public Sub GenerateJson()

    Dim jsonTemplateVariables As New Scripting.Dictionary
    
    Dim row As ListRow
        
    For Each row In ClientVariablesTable.ListRows
        Dim jsonAttributes As Scripting.Dictionary
        Set jsonAttributes = New Scripting.Dictionary
        jsonAttributes.Add "title", row.Range(1)
        jsonAttributes.Add "value", row.Range(2)
        jsonAttributes.Add "type", row.Range(4)
        jsonAttributes.Add "note", row.Range(3)
        jsonTemplateVariables.Add row.Range(5), jsonAttributes
    Next

    Dim jsonWrapper As New Scripting.Dictionary
    jsonWrapper.Add "template_variables", jsonTemplateVariables
    
    Dim enforcementFilemarks As New Collection
    
    For Each row In EnforcementTable.ListRows
        enforcementFilemarks.Add row.Range(1)
    Next
    
    jsonWrapper.Add "enforcement_filemarks", enforcementFilemarks

    JsonWriteToFile ThisWorkbook.Path & "\" & JsonDataFilename, JsonConverter.ConvertToJson(jsonWrapper, Whitespace:=2)

End Sub
