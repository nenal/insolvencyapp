VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ExecutorInitials"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
''
'Just a data structure used in BailiffsLoader class
'
'bailiff's data
''

Public idNumber As String
Public district As String
Public beforeNameTitles As String
Public firstname As String
Public lastname1 As String
Public lastname2 As String
Public afterNameTitles As String
Public street As String
Public city As String
Public telephone As String
Public companyIdNumber As String
Public dataBoxId As String
