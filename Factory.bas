Attribute VB_Name = "Factory"
Public Function NewBailiffsLoaderObject(rangeA1 As Range, strDestTableName As String)
    Set NewBailiffsLoaderObject = New bailiffsLoader
    NewBailiffsLoaderObject.Init rangeA1, strDestTableName

End Function

Public Function NewClientDataServiceObject(objClientVariablesTable As ListObject, objEnforcementTable As ListObject, _
        objClientMailMergeTable As ListObject, strJsonDataFilename As String)
        
    Set NewClientDataServiceObject = New ClientDataService
    NewClientDataServiceObject.Init objClientVariablesTable, objEnforcementTable, _
            objClientMailMergeTable, strJsonDataFilename

End Function

Public Function NewTemplateObject(strWordDocFilepath As String, strMailMergeDataSourceName As String)
    
    Set NewTemplateObject = New Template
    NewTemplateObject.Init strWordDocFilepath, strMailMergeDataSourceName
    
End Function

Public Function NewTemplateListObject(ObjTemplateList As ListBox, objPrinter As printerSelect)

    Set NewTemplateListObject = New templateList
    NewTemplateListObject.Init ObjTemplateList, objPrinter

End Function

Public Function NewPrinterSelectObject(objPrinterList As DropDown)
    Set NewPrinterSelectObject = New printerSelect
    NewPrinterSelectObject.Init objPrinterList
    
End Function
