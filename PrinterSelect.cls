VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "PrinterSelect"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
''
'Loads dropbox with list of printers
'
'@author Radim Nenal
''

Private PrinterList As DropDown

''
'Like constructor, see the Factory module
''
Public Sub Init(objPrinterList As DropDown)
    Set PrinterList = objPrinterList
    PrintersToFormsDropDown

End Sub

Public Sub PrintersToFormsDropDown()
    PrinterList.RemoveAllItems

    Dim activePrinterName As String
    activePrinterName = Left(Application.ActivePrinter, InStr(1, Application.ActivePrinter, " na ", 0) - 1)
    
    Dim activePrinterIndex As Integer: activePrinterIndex = 1

    With CreateObject("WScript.Network")
        Dim x As Long
        For x = 1 To .EnumPrinterConnections.Count Step 2
            If .EnumPrinterConnections(x) = activePrinterName Then
                activePrinterIndex = (x + 1) / 2
            End If
            PrinterList.AddItem .EnumPrinterConnections(x)
        Next
    End With
    
    PrinterList.value = activePrinterIndex
    
End Sub

Public Function GetSelectedPrinterName() As String
    GetSelectedPrinterName = PrinterList.List(PrinterList.value)
    
End Function
