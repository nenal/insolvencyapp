VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Template"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
''
'Object of the class can open, print or update a word document containing mailmerge variables
'
'@author Radim Nenal
''

Private printerName As String
Private wordDocFilepath As String
Private mailMergeDataSourceName As String
Private wordApp As Word.Application

''
'Like constructor, see the Factory module
''
Public Sub Init(strWordDocFilepath As String, strMailMergeDataSourceName As String)

    wordDocFilepath = strWordDocFilepath
    mailMergeDataSourceName = strMailMergeDataSourceName
    printerName = ""

    On Error Resume Next
        Set wordApp = GetObject(, "Word.Application")
    On Error GoTo 0
    If wordApp Is Nothing Then
        Set wordApp = CreateObject("Word.Application")
    End If

End Sub

Public Sub SetPrinter(strPrinterName As String)
    
    printerName = strPrinterName

End Sub

Private Function OpenMailMergeDocument(Visible As Boolean) As Word.Document

    ' Word constants:
    Const wdFormLetters = 0, wdOpenFormatAuto = 0
    Const wdSendToNewDocument = 0, wdDefaultFirstRecord = 1, wdDefaultLastRecord = -16
    
    wordApp.Visible = Visible

    Dim wordDoc As Word.Document 'Word.Document
    Set wordDoc = wordApp.Documents.Open(wordDocFilepath)

    Dim strWorkbookName As String 'from ThisWorkbook.Name...
    strWorkbookName = ThisWorkbook.Path & "\" & ThisWorkbook.Name

    wordDoc.MailMerge.MainDocumentType = wdFormLetters

    wordDoc.MailMerge.OpenDataSource _
            Name:=strWorkbookName, _
            AddToRecentFiles:=False, _
            Revert:=False, _
            Format:=wdOpenFormatAuto, _
            Connection:="Data Source=" & strWorkbookName & ";Mode=Read", _
            SQLStatement:="SELECT * FROM `" & mailMergeDataSourceName & "$`"

    wordDoc.Fields.Update 'this may cause failure
        
    Set OpenMailMergeDocument = wordDoc
    
End Function

Public Sub OpenTemplate()
    
    Set wordDoc = OpenMailMergeDocument(True)

End Sub

Public Sub PrintMailMerge()
    
    Dim previousPrinter As String: previousPrinter = wordApp.ActivePrinter
    If printerName <> "" Then
        wordApp.ActivePrinter = printerName
    End If
    
    Set wordDoc = OpenMailMergeDocument(False)
    
    With wordDoc.MailMerge
        .Destination = wdSendToPrinter
        .SuppressBlankLines = True
        With .DataSource
            .FirstRecord = wdDefaultFirstRecord
            .LastRecord = wdDefaultLastRecord
        End With
        On Error Resume Next
            .Execute Pause:=False
        On Error GoTo 0
    End With

    wordApp.ActivePrinter = previousPrinter

    wordDoc.Close SaveChanges:=False

End Sub

Public Sub UpdateTemplate()

    Set wordDoc = OpenMailMergeDocument(False)

    wordDoc.Close SaveChanges:=wdSaveChanges, OriginalFormat:=wdOriginalDocumentFormat

End Sub

