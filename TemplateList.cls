VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "TemplateList"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
''
'Loads list of templates to the listbox (templates are word documents stored in subfolders)
'
'@author Radim Nenal
''

Private ListBoxTemplates As ListBox
Private ClientTemplatesCount As Integer
Private PrinterSelectObject As printerSelect
Private Const toOPEN As Integer = 1
Private Const toPRINT As Integer = 2
Private Const toSAVE As Integer = 3

''
'Like constructor, see the Factory module
''
Public Sub Init(objListBoxTemplates As ListBox, objPrinter As printerSelect)
    If objListBoxTemplates Is Nothing Or objPrinter Is Nothing Then
        Err.Raise vbObjectError + 1, "InsolvencyApp:TemplateList:Init", "Object variable is null."
    End If
    Set ListBoxTemplates = objListBoxTemplates
    Set PrinterSelectObject = objPrinter
    ListBoxTemplatesInit
    
End Sub

Private Function ListFiles(Optional sFileMask As String = "*", Optional sPath As String = "") As Collection
    If sPath = "" Then
        sPath = ThisWorkbook.Path
    End If

    Dim sFile As String
    Dim aFiles As Collection: Set aFiles = New Collection
    
    sFiles = ""
    
    sFile = Dir(sPath & "\" & sFileMask)
    Do While sFile <> ""
        aFiles.Add sFile
        sFile = Dir 'This moves the value of strFile to the next file.
    Loop

    Set ListFiles = aFiles

End Function

Private Sub ListBoxTemplatesInit()
    ListBoxTemplates.MultiSelect = 2
    ListBoxTemplates.RemoveAllItems

    Dim Files As Collection, it As Variant
    ClientTemplatesCount = 0
    
    Set Files = ListFiles("*.docx", ThisWorkbook.Path & "\VZORY_KLIENT")
    For Each it In Files
        ListBoxTemplates.AddItem it
        ClientTemplatesCount = ClientTemplatesCount + 1
    Next
    
    Set Files = ListFiles("*.docx", ThisWorkbook.Path & "\VZORY_EXEKUCE")
    For Each it In Files
        ListBoxTemplates.AddItem it
    Next
    
End Sub

Private Sub TemplateMassOperation(templateOperation As Integer)
    
    Dim objTemplate As Template
    Dim emforcementTemplatesPath As String: emforcementTemplatesPath = ThisWorkbook.Path & "\VZORY_EXEKUCE\"
    Dim clientTemplatesPath As String: clientTemplatesPath = ThisWorkbook.Path & "\VZORY_KLIENT\"
        
    For Item = 1 To ListBoxTemplates.ListCount
        If ListBoxTemplates.Selected(Item) = True Or templateOperation = toSAVE Then
            If Item > ClientTemplatesCount Then
                Set objTemplate = NewTemplateObject(emforcementTemplatesPath & ListBoxTemplates.List(Item), _
                        "DATA_EXEKUCE")
            Else
                Set objTemplate = NewTemplateObject(clientTemplatesPath & ListBoxTemplates.List(Item), _
                        "DATA_KLIENT")
            End If
            If templateOperation = toOPEN Then
                objTemplate.OpenTemplate
            ElseIf templateOperation = toPRINT Then
                objTemplate.SetPrinter (PrinterSelectObject.GetSelectedPrinterName)
                objTemplate.PrintMailMerge
            ElseIf templateOperation = toSAVE Then
                objTemplate.UpdateTemplate
            End If
        End If
    Next

End Sub

Public Sub OpenTemplates()
    TemplateMassOperation toOPEN
End Sub

Public Sub PrintTemplates()
    TemplateMassOperation toPRINT
End Sub

Public Sub UpdateTemplates()
    MsgBox "Aktualizuji data ve v�ech �ablon�ch. Bude to chv�li trvat."
    TemplateMassOperation toSAVE
    MsgBox "�ablony byly �sp�n� aktualizov�ny."
End Sub
