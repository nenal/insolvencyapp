VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ThisWorkbook"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = True
''
'ThisWorkbook
'
'Microsoft Excel Object
'
'Application entry point
'
'@author Radim Nenal
''

Private ObjClientDataService As ClientDataService                       'provides especially loading and saving user data  (e.g. name, address etc.)
Private ObjTemplateList As templateList                                 'loads list of templates to the listbox (templates are files stored in subfolders)
Private ObjPrinterSelect As printerSelect                               'loads dropbox with list of printers

Const UI_SHEETNAME As String = "Dlu�n�k"                                'sheet with Application UI
Const CLIENT_DATA_SHEETNAME As String = "DATA_KLIENT"                   'name of hidden sheet which serves as mailmerge source
Const FORMS_DROPDOWN_LIST_OF_PRINTERS As String = "ddListOfPrinters"    'MSForms DropDown name
Const TBL_TEMPLATE_VARIABLES As String = "T_dlu�n�k_vstup"              'ListObject name
Const TBL_ENFORCEMENT_FILEMARKS As String = "Exekuce_vstup"             'ListObject name
Const TBL_CLIENT_DATA As String = "data_dlu�n�k"                        'ListObject name
Const JSONDATA_FILENAME As String = "data.json"                         'file to store user data (e.g. name, address etc.)

Private Function GetClientDataServiceObject() As ClientDataService
    If ObjClientDataService Is Nothing Then
        Set GetClientDataServiceObject = NewClientDataServiceObject( _
                Worksheets(UI_SHEETNAME).ListObjects(TBL_TEMPLATE_VARIABLES), _
                Worksheets(UI_SHEETNAME).ListObjects(TBL_ENFORCEMENT_FILEMARKS), _
                Worksheets(CLIENT_DATA_SHEETNAME).ListObjects(TBL_CLIENT_DATA), _
                JSONDATA_FILENAME)
    Else
        Set GetClientDataServiceObject = ObjClientDataService
    End If

End Function

Private Function GetPrinterSelectObject() As printerSelect
    If ObjPrinterSelect Is Nothing Then
        Dim objPrinterList As DropDown
        Set objPrinterList = Worksheets(UI_SHEETNAME).Shapes(FORMS_DROPDOWN_LIST_OF_PRINTERS).OLEFormat.Object
        Set GetPrinterSelectObject = NewPrinterSelectObject(objPrinterList)
    Else
        Set GetPrinterSelectObject = ObjPrinterSelect
    End If
    
End Function

Private Function GetTemplateListObject() As templateList
    If ObjTemplateList Is Nothing Then
        Dim objListBoxTemplates As ListBox
        Set objListBoxTemplates = Worksheets(UI_SHEETNAME).Shapes("lbClientTemplates").OLEFormat.Object
        Set GetTemplateListObject = NewTemplateListObject(objListBoxTemplates, GetPrinterSelectObject())
    Else
        Set GetTemplateListObject = ObjTemplateList
    End If
    
End Function

Private Sub Workbook_Open()
    Set ObjClientDataService = Nothing
    Set ObjTemplateList = Nothing
    Set ObjPrinterSelect = Nothing
    Application.ScreenUpdating = False
    Set ObjClientDataService = GetClientDataServiceObject()
    ObjClientDataService.LoadJson
    Set ObjTemplateList = GetTemplateListObject()
    ThisWorkbook.RefreshAll
    Application.ScreenUpdating = True

End Sub

Private Sub Workbook_BeforeSave(ByVal SaveAsUI As Boolean, Cancel As Boolean)
    GetClientDataServiceObject().GenerateJson

End Sub

Private Sub Run_Workbook_BeforeSave()
    Workbook_BeforeSave False, False

End Sub

Private Sub btnRefreshAll()
    Workbook_Open
    GetClientDataServiceObject().GenerateJson
    
End Sub

Private Sub btnRemoveLastRow()
    Dim Tbl As ListObject
    Set Tbl = Worksheets(UI_SHEETNAME).ListObjects("Exekuce_vstup")
    Tbl.ListRows(Tbl.DataBodyRange.Rows.Count).Delete
    
End Sub

Private Sub btnAddRow()
    Dim Tbl As ListObject
    Set Tbl = Worksheets(UI_SHEETNAME).ListObjects("Exekuce_vstup")
    Dim newrow As ListRow
    Set newrow = Tbl.ListRows.Add
    With newrow
    
    End With

End Sub

Private Sub btnPrintTemplates()
    GetTemplateListObject().PrintTemplates
    
End Sub

Private Sub btnTemplatesSaveChanges()
    GetTemplateListObject().UpdateTemplates
    
End Sub

Private Sub btnOpenTemplates()
    GetTemplateListObject().OpenTemplates
    
End Sub

Private Sub btnLoadEKCR()
    MsgBox "Na��t�m seznam exekutor� z internetu. Chvilinku to m��e trvat."
    Dim bailiffs As bailiffsLoader
    Set bailiffs = NewBailiffsLoaderObject(Worksheets("ekcr_seznam").Range("A1"), "tblExekutori_1")
    ThisWorkbook.RefreshAll
    MsgBox "Seznam exekutor� byl �sp�n� aktualizov�n."
    
End Sub


